'use strict';

var innerProfileTranslating = profileTranslating();

window.onload = function () {
    navigationTabs();
    sizePhoto();
    // windowWidth();
    innerProfileTranslating.windowWidth();

    hoverPhoto();
    showDate();
};

window.onresize = debounce(function () {
    innerProfileTranslating.arrowVisibility();
}, 200);

//polyfill Nodelist.prototype.forEach
if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
        thisArg = thisArg || window;
        for (var i = 0; i < this.length; i++) {
            callback.call(thisArg, this[i], i, this);
        }
    };
}

//polyfill Array.prototype.forEach
if (!Array.prototype.forEach) {
    Array.prototype.forEach = function (fun /*, thisp*/) {
        var len = this.length;
        if (typeof fun != "function") throw new TypeError();

        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in this) fun.call(thisp, this[i], i, this);
        }
    };
}

function navigationTabs() {
    var tabs = document.querySelectorAll('.c-menu > li > a');

    function tabActive() {
        tabs.forEach(function (link) {
            return link.classList.remove('active');
        });
        this.classList.add('active');
    }

    tabs.forEach(function (link) {
        return link.addEventListener('click', tabActive);
    });
}

function sizePhoto() {
    var thumbnail = document.querySelectorAll('.o-profile--photo');
    var profileContainer = document.getElementById("js-profile__container");

    function getPhotoLink() {
        thumbnail.forEach(function (photo) {
            return photo.classList.remove('sharp');
        });
        this.classList.add('sharp');

        var noNumber = this.dataset.number;
        var imgSrc = this.currentSrc || this.src;
        var partialSrc = imgSrc.substring(0, imgSrc.length - 4);

        profileContainer.style.backgroundImage = 'url(' + partialSrc + '-bg.jpg)';
        profileContainer.dataset.currentNo = '' + noNumber;
    }

    thumbnail.forEach(function (photo) {
        return photo.addEventListener('click', getPhotoLink);
    });
}

function hoverPhoto() {
    var profileInfo = document.getElementById('js-profile__info');
    var profileContainer = document.getElementById("js-profile__container");

    var withoutJS = document.querySelectorAll('.css-active');
    withoutJS.forEach(function (className) {
        return className.classList.remove('css-active');
    });

    function showInfo() {
        var currentNumber = this.dataset.currentNo || 3;
        var active = document.querySelector('[data-number=\'' + currentNumber + '\']');
        var name = active.nextElementSibling;
        var position = name.nextElementSibling;

        var nodeNameCopy = name.cloneNode(true);
        var nodePositionCopy = position.cloneNode(true);

        profileInfo.appendChild(nodeNameCopy);
        profileInfo.appendChild(nodePositionCopy);
    }

    function hideInfo() {
        while (profileInfo.firstChild) {
            profileInfo.removeChild(profileInfo.firstChild);
        }
    }

    profileContainer.addEventListener('mouseover', showInfo);
    profileContainer.addEventListener('mouseout', hideInfo);
}

function profileTranslating() {
    var bundle = {};
    var arrowLeft = document.getElementById('js-arrowLeft');
    var arrowRight = document.getElementById('js-arrowRight');
    var rowOfImage = document.getElementById("js-transforming");
    var counter = 0;

    bundle.leftTransform = function () {
        var counterLeft = counter + 1;
        var xValue = counterLeft * 64;

        rowOfImage.style.transform = 'translate(' + xValue + 'px)';
        counter = counterLeft;
        bundle.arrowVisibility();
    };

    bundle.rightTransform = function () {
        var counterRight = counter - 1;
        var xValue = counterRight * 64;

        rowOfImage.style.transform = 'translate(' + xValue + 'px)';
        counter = counterRight;
        bundle.arrowVisibility();
    };

    arrowLeft.addEventListener('click', bundle.leftTransform);
    arrowRight.addEventListener('click', bundle.rightTransform);

    bundle.windowWidth = function () {
        if (window.innerWidth >= 768) {
            return -4;
        } else {
            return -7;
        }
    };

    bundle.arrowVisibility = function () {
        var limitCounter = bundle.windowWidth();
        var currentCounter = counter;

        switch (true) {
            case 0 <= currentCounter:
                arrowLeft.style.display = 'none';
                rowOfImage.style.transform = 'translate(0px)';
                return counter = 0;
                break;
            case limitCounter >= currentCounter:
                arrowRight.style.display = 'none';
                rowOfImage.style.transform = 'translate(' + limitCounter * 64 + 'px)';
                return counter = limitCounter;
                break;
            default:
                arrowLeft.style.display = 'block';
                arrowRight.style.display = 'block';
        }
    };

    bundle.arrowVisibility();
    return {
        arrowVisibility: bundle.arrowVisibility,
        windowWidth: bundle.windowWidth
    };
}

function showDate() {
    var date = new Date();
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var day = date.getUTCDate();
    var month = months[date.getMonth()];
    var year = date.getFullYear();
    var dateNode = document.createElement("P");
    var textnode = document.createTextNode(day + ' ' + month + ' ' + year);

    dateNode.classList.add('c-header__date', 'c-header__dates');
    dateNode.appendChild(textnode);
    document.getElementById("js-header").appendChild(dateNode);
}

function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this,
            args = arguments;

        var later = function later() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}