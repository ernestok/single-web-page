'use strict';

const innerProfileTranslating = profileTranslating();

window.onload = function () {
    navigationTabs();
    sizePhoto();
    innerProfileTranslating.windowWidth();
    hoverPhoto();
    showDate();
};


window.onresize = debounce(function () {
    innerProfileTranslating.arrowVisibility()
}, 200);


//polyfill Nodelist.prototype.forEach
if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
        thisArg = thisArg || window;
        for (var i = 0; i < this.length; i++) {
            callback.call(thisArg, this[i], i, this);
        }
    };
}

//polyfill Array.prototype.forEach
if (!Array.prototype.forEach) {
    Array.prototype.forEach = function (fun /*, thisp*/) {
        var len = this.length;
        if (typeof fun != "function")
            throw new TypeError();

        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in this)
                fun.call(thisp, this[i], i, this);
        }
    };
}

function navigationTabs() {
    const tabs = document.querySelectorAll('.c-menu > li > a');

    function tabActive() {
        tabs.forEach(link => link.classList.remove('active'));
        this.classList.add('active');
    }

    tabs.forEach(link => link.addEventListener('click', tabActive));
}

function sizePhoto() {
    const thumbnail = document.querySelectorAll('.o-profile--photo');
    const profileContainer = document.getElementById("js-profile__container");

    function getPhotoLink() {
        thumbnail.forEach(photo => photo.classList.remove('sharp'));
        this.classList.add('sharp');

        const noNumber = this.dataset.number;
        const imgSrc = this.currentSrc || this.src;
        const partialSrc = imgSrc.substring(0, (imgSrc.length - 4));

        profileContainer.style.backgroundImage = `url(${partialSrc}-bg.jpg)`;
        profileContainer.dataset.currentNo=`${noNumber}`;
    }

    thumbnail.forEach(photo => photo.addEventListener('click', getPhotoLink));
}

function hoverPhoto() {
    const profileInfo = document.getElementById('js-profile__info');
    const profileContainer = document.getElementById("js-profile__container");

    const withoutJS = document.querySelectorAll('.css-active');
    withoutJS.forEach(className => className.classList.remove('css-active'));

    function showInfo() {
        const currentNumber = this.dataset.currentNo || 3;
        const active = document.querySelector(`[data-number='${currentNumber}']`);
        const name = active.nextElementSibling;
        const position = name.nextElementSibling;

        const nodeNameCopy = name.cloneNode(true);
        const nodePositionCopy = position.cloneNode(true);

        profileInfo.appendChild(nodeNameCopy);
        profileInfo.appendChild(nodePositionCopy);
    }

    function hideInfo() {
        while (profileInfo.firstChild) {
            profileInfo.removeChild(profileInfo.firstChild);
        }
    }

    profileContainer.addEventListener('mouseover', showInfo);
    profileContainer.addEventListener('mouseout', hideInfo);

}

function profileTranslating() {
    const bundle = {};
    const arrowLeft = document.getElementById('js-arrowLeft');
    const arrowRight = document.getElementById('js-arrowRight');
    const rowOfImage = document.getElementById("js-transforming");
    let counter = 0;

    bundle.leftTransform = function () {
        const counterLeft = counter + 1;
        const xValue = counterLeft * 64;

        rowOfImage.style.transform = `translate(${xValue}px)`;
        counter = counterLeft;
        bundle.arrowVisibility()
    };

    bundle.rightTransform = function () {
        const counterRight = counter - 1;
        const xValue = counterRight * 64;

        rowOfImage.style.transform = `translate(${xValue}px)`;
        counter = counterRight;
        bundle.arrowVisibility();
    };

    arrowLeft.addEventListener('click', bundle.leftTransform);
    arrowRight.addEventListener('click', bundle.rightTransform);

    bundle.windowWidth = function () {
        if (window.innerWidth >= 768) {
            return -4;
        } else {
            return -7;
        }
    };

    bundle.arrowVisibility = function () {
        let limitCounter = bundle.windowWidth();
        let currentCounter = counter;

        switch (true) {
            case (0 <= currentCounter):
                arrowLeft.style.display = 'none';
                rowOfImage.style.transform = `translate(0px)`;
                return counter = 0;
                break;
            case (limitCounter >= currentCounter):
                arrowRight.style.display = 'none';
                rowOfImage.style.transform = `translate(${limitCounter * 64}px)`;
                return counter = limitCounter;
                break;
            default:
                arrowLeft.style.display = 'block';
                arrowRight.style.display = 'block';
        }
    };

    bundle.arrowVisibility();
    return {
        arrowVisibility: bundle.arrowVisibility,
        windowWidth: bundle.windowWidth
    };
}

function showDate() {
    const date = new Date();
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const day = date.getUTCDate();
    const month = months[date.getMonth()];
    const year = date.getFullYear();
    const dateNode = document.createElement("P");
    const textnode = document.createTextNode(`${day} ${month} ${year}`);

    dateNode.classList.add('c-header__date', 'c-header__dates');
    dateNode.appendChild(textnode);
    document.getElementById("js-header").appendChild(dateNode);
}

function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;

        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}
