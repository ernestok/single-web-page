# Read Me #

## This single web page project include: ##

    1. Updatating current date.
    2. Self made carousel with:
        - The “previous” and “next” arrows (on the left and right of the thumbnails) scroll the images left and right,
        - The “previous” arrow to the left of the thumbnail images will not show if there are no previous images,
        - The “next” arrow to the right of the thumbnail images will not show if there are no more images,
        - When a user selects an image, it shows in the main image area,
        - The information (name and job title) on the large image only display when the mouse is over the image    
   


## Author information ##

Ernest  Kost

ernest.kost@gmail.com



## Installation and launching ##


### 1. Installing npm packages ###

  In order to launch the app, firstly you need to install the required npm packages. 
  Change your directory to folder with gulfile.js and use the command:
  
  `npm install`
  
  
### 2. Launching the app in production environment ###

  In order to see website in developer environment, use commmand:
  
  `npm start`
  
  
### 3. Creating files for distribution ###

  In order to produce files ready to upload on server, you need to create them by using command:
  
  `npm run build`
  
  Command will delete all of content inside the dist folder and will generate new in the same location minified and concatenated webside ready to be deployed on server.
  
  
###  Used tools and frameworks ###

  
  - vanillaJS including ES6
  - [Babel compiler](https://babeljs.io/)
  - [SASS](http://sass-lang.com/)
  
  